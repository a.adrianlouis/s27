// User Collection
{
	"id":"buyer-01",
	"firstName": "John",
	"lastName": "Doe",
	"email": "johndoe@mail.com",
	"password": "john123",
	"isAdmin": false,
	"mobileNumber": "09123456789"
	
}

// Orders Collection 
{
	"id":"order-01",
	"user_id": "buyer-01", 
	"transactionDate": "04/18/2023",
	"status": "",
	"total": 25
}

// Products Collection 
{
	"id":"product-01",
	"name": "HTML",
	"description": "Intro to Web Development",
	"price": 150,
	"stocks": 500,
	"isActive": true,
	"SKU": 
}

// Order Products Collection
{
	"id": "order_prodct-001",
	"order_id": "order-01", 
	"product_id": "product-01",
	"quantity": 6,
	"price": 150,
	"subTotal": 900
}